# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Servicios ###

* Documentos servicios en swagger
* http://localhost:8080/swagger-ui.html#/

### Implementaciones ###
* Servicios suma, resta e historico (ver swagger)
    * Valor mínimo = 1 de los parametros a modo de demostración de v    alidaciones en el dto
* Interceptor para poner trazas del tiempo transcurrido en cada petición
* Manejador de excepciones:
    * Validaciones implementadas en el dto
    * Excepción de parámetros de entrada
    * Excepción genérica
* Configuración de properties para externalizar textos
* Configuración del swagger
* Test JUnit
* Se traza el resultado mediante TracerAPI en un jsonFilter
    * Se incluye tracer-1.0.0.jar al no tener acceso a su repositry