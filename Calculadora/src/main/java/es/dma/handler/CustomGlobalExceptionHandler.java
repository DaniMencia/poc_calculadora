package es.dma.handler;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import es.dma.dto.ApiError;

@RestControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(CustomGlobalExceptionHandler.class);
	
	@Autowired
	private Environment env;
	
	@ExceptionHandler({ ConstraintViolationException.class })
	public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
	    
		logger.info("CustomGlobalExceptionHandler: handleConstraintViolation() INI");
		
		List<String> errors = new ArrayList<String>();
	    for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
	        errors.add(violation.getMessage());
	    }
	    ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, errors);
	    
	    logger.info("CustomGlobalExceptionHandler: handleConstraintViolation() FIN");	    
	    	    
	    return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
	}
	
	@ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handle(Exception ex, HttpServletRequest request, HttpServletResponse response) {
		
		logger.info("CustomGlobalExceptionHandler: handle() INI");
		
		List<String> errors = new ArrayList<String>();
        errors.add(env.getProperty("error.interno"));
        ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, errors);
        
        logger.info("CustomGlobalExceptionHandler: handle() FIN");
        
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
    }
	
	@Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus httpStatus, WebRequest request) {

		logger.info("CustomGlobalExceptionHandler: handleHttpMessageNotReadable() INI");
		
        List<String> errors = new ArrayList<String>();
        errors.add(env.getProperty("error.formato.parametros"));
		//errors.add(ex.getMessage()); Solo si se quiere dar información de la expcepción del servidor
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, errors);
		
		logger.info("CustomGlobalExceptionHandler: handleHttpMessageNotReadable() FIN");
		
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
    }

}
