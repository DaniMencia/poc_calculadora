package es.dma.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component
public class TiempoTranscurridoInterceptor implements HandlerInterceptor {
	
	private static final Logger logger = LoggerFactory.getLogger(TiempoTranscurridoInterceptor.class);
	
	@Autowired
	private Environment env;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		//true se sigue con la ejecución
		//false se termina la ejecución (o se puede redirigir)
		logger.info("TiempoTranscurridoInterceptor: preHandle() ".concat(env.getProperty("trazas.ini")));
		logger.info("Interceptando " +handler);
		long tiempoInicio = System.currentTimeMillis();
		request.setAttribute("tiempoInicio", tiempoInicio);
		
		logger.info("TiempoTranscurridoInterceptor: preHandle() ".concat(env.getProperty("trazas.fin")));
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
		logger.info("TiempoTranscurridoInterceptor: postHandle() ".concat(env.getProperty("trazas.ini")));
		
		long tiempoIniio = (Long) request.getAttribute("tiempoInicio");
		long tiempoFin = System.currentTimeMillis();
		long tiempoTranscurrido = tiempoFin - tiempoIniio;
		
		logger.info("TiempoTranscurridoInterceptor: postHandle() tiempoTranscurrido = " +tiempoTranscurrido + " ms");
		logger.info("TiempoTranscurridoInterceptor: postHandle() ".concat(env.getProperty("trazas.fin")));
	}
	
	

}

