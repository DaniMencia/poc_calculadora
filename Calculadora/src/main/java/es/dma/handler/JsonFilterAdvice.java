package es.dma.handler;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.List;

import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.fasterxml.jackson.annotation.JsonFilter;

import es.dma.dto.OperacionesDto;
import io.corp.calculator.TracerImpl;

@ControllerAdvice
public class JsonFilterAdvice implements ResponseBodyAdvice<OperacionesDto> {
	
	private final TracerImpl tracer = new TracerImpl();
    
	@Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		List<Annotation> annotations = Arrays.asList(returnType.getMethodAnnotations());
        return annotations.stream().anyMatch(annotation -> annotation.annotationType().equals(JsonFilter.class));
    }

    @Override
    public OperacionesDto beforeBodyWrite(OperacionesDto body, MethodParameter returnType, MediaType selectedContentType, Class<? extends
    HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
    	tracer.trace(body.getResultado());
        return body;
    }
}