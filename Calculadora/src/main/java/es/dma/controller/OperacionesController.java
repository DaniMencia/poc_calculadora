package es.dma.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFilter;

import es.dma.dto.OperacionesDto;
import es.dma.service.OperacionesService;

@RestController
@Validated
@RequestMapping("/calculadora")
public class OperacionesController {
	
	@Autowired
	OperacionesService operacionesService;
	
	@PostMapping("/suma")
	@JsonFilter("JsonFilterAdvice")
	public ResponseEntity<OperacionesDto> suma(@RequestBody @Valid OperacionesDto operacionesDto, BindingResult result) {
		ResponseEntity<OperacionesDto> response = new ResponseEntity(operacionesService.suma(operacionesDto),HttpStatus.OK);
		return response;		
	}
	
	@PostMapping("/resta")
	@JsonFilter("JsonFilterAdvice")
	public ResponseEntity<OperacionesDto> resta(@RequestBody @Valid OperacionesDto operacionesDto, BindingResult result) {
		ResponseEntity<OperacionesDto> response = new ResponseEntity(operacionesService.resta(operacionesDto),HttpStatus.OK);
		return response;		
	}
	
	@GetMapping("/historico")
	public ResponseEntity<List<OperacionesDto>> historico() {
		List<OperacionesDto> respuesta = operacionesService.histroico();	
		ResponseEntity<List<OperacionesDto>> response = new ResponseEntity(respuesta,HttpStatus.OK);
		return response;		
	}

}
