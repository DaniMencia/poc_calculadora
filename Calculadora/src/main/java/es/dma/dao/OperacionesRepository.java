package es.dma.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import es.dma.dto.OperacionesDto;

public interface OperacionesRepository extends JpaRepository<OperacionesDto, Long> {

}
