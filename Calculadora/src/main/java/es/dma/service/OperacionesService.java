package es.dma.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import es.dma.dao.OperacionesRepository;
import es.dma.dto.OperacionesDto;

@Service
public class OperacionesService {
	
	@Autowired
	private Environment env;
	
	@Autowired
	OperacionesRepository operacionesRepository;

	public OperacionesDto suma(OperacionesDto operacionesDto) {
		operacionesDto.setOperacion(env.getProperty("operacion.suma"));
		operacionesDto.setResultado(operacionesDto.getParam1()+operacionesDto.getParam2());
		guardarOper(operacionesDto);
		return operacionesDto;
	}
	
	public OperacionesDto resta(OperacionesDto operacionesDto) {
		operacionesDto.setOperacion(env.getProperty("operacion.resta"));
		operacionesDto.setResultado(operacionesDto.getParam1()-operacionesDto.getParam2());
		guardarOper(operacionesDto);
		return operacionesDto;
	}
	
	private void guardarOper(OperacionesDto operacionesDto) {
		operacionesRepository.saveAndFlush(operacionesDto);
	}
	
	public List<OperacionesDto> histroico() {
		return operacionesRepository.findAll();
	}
}
