package es.dma.dto;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class OperacionesDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@JsonIgnore
	Long id;
	
	String operacion;
	
	@Min(value = 1, message = "{error.minimo.parametreo1}")
	int param1;	
	
	@Min(value = 1, message = "{error.minimo.parametreo2}")
	int param2;
	
	int resultado;
	
	public OperacionesDto() {
		super();
	}
	
	public OperacionesDto(String operacion, int param1, int param2, int resultado) {
		super();
		this.operacion = operacion;
		this.param1 = param1;
		this.param2 = param2;
		this.resultado = resultado;
	}
	
	public int getParam1() {
		return param1;
	}
	public void setParam1(int param1) {
		this.param1 = param1;
	}
	public int getParam2() {
		return param2;
	}
	public void setParam2(int param2) {
		this.param2 = param2;
	}
	public int getResultado() {
		return resultado;
	}
	public void setResultado(int resultado) {
		this.resultado = resultado;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	
}
