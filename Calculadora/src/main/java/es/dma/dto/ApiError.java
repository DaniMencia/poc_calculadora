package es.dma.dto;

import java.io.Serializable;
import java.util.List;

import org.springframework.http.HttpStatus;

public class ApiError implements Serializable {
	 
	private static final long serialVersionUID = 2L;
	
	private HttpStatus status;
    private List<String> errors;
 
    public ApiError(HttpStatus status, List<String> errors) {
        super();
        this.status = status;
        this.errors = errors;
    }
    
    public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
}
