package es.dma;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@TestPropertySource(locations = "classpath:testJunit.properties")
public class OperacionesTest extends CalculadoraApplicationTests{
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Value("${operacion.ok}")
	private String mockOK;
	
	@Value("${operacion.ko1.un.parametro}")
	private String mockSoloUnParametro;
	
	@Value("${operacion.ko2.decimales}")
	private String mockSDecimales;
	
	@Value("${operacion.ko3.letras}")
	private String mockLetras;
	
	@Value("${operacion.ko4.nullnull}")
	private String mockNull;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}
	
	// TEST OPERACION SUMA
	
	@Test
	public void testSumaOK() throws Exception {
		mockMvc.perform(post("/calculadora/suma")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mockOK))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.resultado").value("8"));
	}
	
	@Test
	public void testSumaSoloUnParametro() throws Exception {
		mockMvc.perform(post("/calculadora/suma")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mockSoloUnParametro))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testSumaDecimales() throws Exception {
		mockMvc.perform(post("/calculadora/suma")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mockSDecimales))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testSumaLetras() throws Exception {
		mockMvc.perform(post("/calculadora/suma")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mockLetras))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testSumaNull() throws Exception {
		mockMvc.perform(post("/calculadora/suma")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mockNull))
				.andExpect(status().isBadRequest());
	}
	
	// TEST OPERACION RESTA
	
	@Test
	public void testRestaOK() throws Exception {
		mockMvc.perform(post("/calculadora/resta")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mockOK))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.resultado").value("-2"));
	}
	
	@Test
	public void testRestaSoloUnParametro() throws Exception {
		mockMvc.perform(post("/calculadora/resta")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mockSoloUnParametro))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testRestaDecimales() throws Exception {
		mockMvc.perform(post("/calculadora/resta")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mockSDecimales))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testRestaLetras() throws Exception {
		mockMvc.perform(post("/calculadora/resta")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mockLetras))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testRestaNull() throws Exception {
		mockMvc.perform(post("/calculadora/resta")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mockNull))
				.andExpect(status().isBadRequest());
	}
	
}
